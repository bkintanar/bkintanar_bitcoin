<html>
	<head>
		<title>Bastard</title>
    	<link href="/css/bootstrap.min.css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" media="screen" href="/css/bootstrap-datetimepicker.min.css">	</head>
	<body class="container"> 
	    <div style="margin-left:auto; margin-right:auto;"> 
	        <h5>filter bitcoin ticker data by date range:</h5>
	        <div> 
	            <form action="results.php" method="post">
	                <div class="well">
	                	From:
		                <div id="from" class="input-append date">
		                	<input type="text" placeholder="yyyy-mm-dd hh:mm:ss" name="from" style="height:30px">
		                	<span class="add-on">
						      	<i data-time-icon="icon-time" data-date-icon="icon-calendar">
						      	</i>
						    </span>
		                </div>
		            	To:
		                <div id="to" class="input-append date">
		                	<input type="text" placeholder="yyyy-mm-dd hh:mm:ss" name="to" style="height:30px">
		                	<span class="add-on">
						      	<i data-time-icon="icon-time" data-date-icon="icon-calendar">
						      	</i>
						    </span>
		                </div>
		                <button type="submit" class="btn green">Submit</button>
	                </div>
	            </form>  
	        </div>
	    </div>
		<script type="text/javascript" src="/js/jquery.min.js"></script>
		<script type="text/javascript" src="/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="/js/bootstrap-datetimepicker.min.js"></script>
	    <script type="text/javascript"> 
	        $(function() {
	            $('#from, #to').datetimepicker({format: 'yyyy-MM-dd hh:mm:ss'}); 
	        });   
	    </script>
	</body>
</html>