<?php

	require_once('bitcoin.php');

	if (empty($_POST))
	{
		header('Location: /index.php');
	}

	$date_range = array('from' => $_POST['from'], 'to' => $_POST['to']);

	$_bitcoin = new Bitcoin($date_range);

	$_lowestLOW = $_bitcoin->getLowestLOW();
	$_lowestMP  = $_bitcoin->getLowestMP();
	$_highestMP = $_bitcoin->getHighestMP();
	$_highestHIGH = $_bitcoin->gethighestHIGH();
	$_currentMPValues = $_bitcoin->getCurrentMPValues();
	$_MPAverage = $_bitcoin->getMPAvg();

?>

<html>
	<head>
    	<title>John Snow</title>
    	<link rel="stylesheet" href="/css/bootstrap.min.css" />
	</head>
	<body class="container">
		<div style="margin-left:auto; margin-right:auto;"> 
			<div>
				<br /><br />Values are based on the date range <strong><?php echo $date_range['from'] ?></strong> to <strong><?php echo $date_range['to'] ?></strong><br /><br />
			</div>
			<div>
				<table class="table">
					<tbody>
						<tr>
							<td>Lowest Low:</td>
							<td><?php echo $_lowestLOW->value ?></td>
							<td><?php echo $_lowestLOW->date_format ?></td>
						</tr>
						<tr>
							<td>Lowest Market Price:</td>
							<td><?php echo $_lowestMP->value ?></td>
							<td><?php echo $_lowestMP->date_format ?></td>
						</tr>
						<tr>
							<td>Highest Market Price:</td>
							<td><?php echo $_highestMP->value ?></td>
							<td><?php echo $_highestMP->date_format ?></td>
						</tr>
						<tr>
							<td>Highest High:</td>
							<td><?php echo $_highestHIGH->value ?></td>
							<td><?php echo $_highestHIGH->date_format ?></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td>Current Market Price:</td>
							<td><?php echo $_currentMPValues->buy ?></td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td>Current High:</td>
							<td><?php echo $_currentMPValues->high ?></td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td>Current Low:</td>
							<td><?php echo $_currentMPValues->low ?></td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td colspan="3">
								Average Market Price from date range 
								<strong><?php echo $date_range['from'] ?></strong> to 
								<strong><?php echo $date_range['to'] ?></strong> is <?php echo $_MPAverage ?>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</body>
</html>