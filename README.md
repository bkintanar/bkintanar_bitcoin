bkintanar_bitcoin
=========

bkintanar_bitcoin is written by Bertrand Kintanar and is based on the exam.txt file sent by Rob Maynard.

What it does
=========

* It retrieves data from `https://btc-e.com/api/2/btc_usd/ticker` and stores it to the database `bitcoin` on the table `bitcoin_ticker`.

* The site then allows the user to select date range.

* Depending on the date range supplied by the user, data displays as result.

Disclaimer
=========

* There were no instructions on what date format to use in displaying the result. I took the liberty in using the `%b %d, %Y %H:%i` MySQL date format.

* There were no sample output given as to what the output should look like. The only given is what to output so I did all I can to display the data as clear as posible.

Steps needed in the Development Environment
=========

* Create a database named `bitcoin`.

* Create a database user and password of `bitcoin` and grant access to the database `bitcoin`.

* Create a table named `bitcoin_ticker` using the `SQL CREATE STATEMENT` below.

```sql
CREATE TABLE `bitcoin_ticker` (
    `high` decimal(11,0) DEFAULT NULL,
    `low` decimal(11,0) DEFAULT NULL,
    `avg` decimal(11,0) DEFAULT NULL,
    `vol` decimal(11,0) DEFAULT NULL,
    `vol_cur` decimal(11,0) DEFAULT NULL,
    `last` decimal(11,0) DEFAULT NULL,
    `buy` decimal(11,0) DEFAULT NULL,
    `sell` decimal(11,0) DEFAULT NULL,
    `updated` int(11) DEFAULT NULL,
    `updated_datetime` datetime DEFAULT NULL,
    `server_time` int(11) DEFAULT NULL,
    `high_low_diff` decimal(11,0) DEFAULT NULL,
    `high_last_market_diff` decimal(11,0) DEFAULT NULL,
    `low_last_market_diff` decimal(11,0) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
```

* Clone the `git` repo from `bitbucket` by using the command below.

```sh
git clone https://bkintanar@bitbucket.org/bkintanar/bkintanar_bitcoin.git
```

* The above command will create a `bkintanar_bitcoin` directory in your `pwd`. Move the `bkintanar_bitcoin` directory to your htdocs or web root directory.

* Edit your `/etc/hosts` file. This might need root access. So you might need to ba `sudoer` and add the entry of `127.0.0.1 bitcoin.loc`.

* Create or edit your apache vhost file and add the following:
```
<VirtualHost *:80>
  ServerName bitcoin.loc
  DocumentRoot "/path/to/bkintanar_bitcoin"
  DirectoryIndex index.php index.html
  <Directory "/path/to/bkintanar_bitcoin">
    Options All
    AllowOverride All
    Allow from All
  </Directory>
</VirtualHost>
```

* Restart apache

* Edit your crontab by typing in `crontab -e` and add the following crontab entry.

```
*/15 * * * * php /path/to/bkintanar_bitcoin/store_bitcoin_to_db.php
```

* Open your favorite web browser and type in `http://bitcoin.loc/` in the address bar and you should be good to go.

* Cheers!