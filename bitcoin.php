<?php

class Bitcoin {
	
	private $db_username = 'bitcoin';
	private $db_password = 'bitcoin';
	private $db_name 	 = 'bitcoin';

    private $date_from = null;
    private $date_to   = null;

    public function __construct($date_range = array())
    {
        $this->date_from = array_key_exists('from', $date_range) ? $date_range['from'] : null;
        $this->date_to   = array_key_exists('to', $date_range)   ? $date_range['to']   : null;
    }

    public function getDateFrom()
    {
        return $this->date_from;
    }

    public function getDateTo()
    {
        return $this->date_to;
    }

	public function getBitcoinTickerObject()
	{
        $json = file_get_contents('https://btc-e.com/api/2/btc_usd/ticker');

        return json_decode($json)->ticker;   
    }

    public function getConnection()
    {
	    try
	    { 
	       return new PDO("mysql:host=localhost;dbname={$this->db_name}", $this->db_username, $this->db_password); 
	    }
	    catch(PDOException $e)
	    {
	        die('ERROR: ' . $e->getMessage());
	    }
    }

    public function storeJSONToDB()
    {
    	$_ticker = $this->getBitcoinTickerObject();

    	$_high        = $_ticker->high;
    	$_low         = $_ticker->low;
    	$_avg         = $_ticker->avg;
    	$_vol         = $_ticker->vol;
    	$_vol_cur     = $_ticker->vol_cur;
    	$_last        = $_ticker->last;
    	$_buy         = $_ticker->buy;
    	$_sell        = $_ticker->sell;
    	$_updated     = $_ticker->updated;
    	$_server_time = $_ticker->server_time;

    	try
    	{
    		$_conn = $this->getConnection();

    		$_statement = $_conn->prepare('INSERT INTO bitcoin_ticker VALUES(
    			:high, :low, :avg, :vol, :vol_cur, :last, :buy, :sell, 
    			:updated, FROM_UNIXTIME(:updated_datetime), :server_time, 
    			:high_low_diff, :high_last_market_diff, :low_last_market_diff);');

    		$_values = array(
    			':high' => $_high, ':low' => $_low, ':avg' => $_avg, 
    			':vol' => $_vol, ':vol_cur' => $_vol_cur, ':last' => $_last, 
    			':buy' => $_buy, ':sell' => $_sell, ':updated' => $_updated, 
    			':updated_datetime' => $_updated, ':server_time' => $_server_time, 
    			':high_low_diff' => $_high - $_low, 
    			':high_last_market_diff' => $_high - $_last, 
    			':low_last_market_diff' => $_low - $_last);

    		$_statement->execute($_values);

    		print('Inserted' . PHP_EOL);

    		$_conn = null;
    	}
    	catch(PDOException $e)
    	{
    		die('PDO ERROR: ' . $e->getMessage());
    	}
    }

    public function getLowestLOW()
    {
        try
        {
            $_conn = $this->getConnection();

            $_statement = $_conn->prepare(
                "SELECT low, DATE_FORMAT(updated_datetime, '%b %d, %Y %H:%i') AS datetime"
                . ' FROM `bitcoin_ticker`'
                . ' WHERE low = (SELECT MIN(low) AS lowest_low'
                    . ' FROM `bitcoin_ticker` WHERE updated_datetime > :from'
                    . ' AND updated_datetime < :to)'
                .' ORDER BY updated DESC LIMIT 1');

            $_values = array(
                ':from' => $this->date_from,
                ':to'   => $this->date_to);

            $_statement->execute($_values);
            $_row = $_statement->fetch();

            $_conn = null;

            $_lowestLOW = new stdClass();
            $_lowestLOW->date_format  = $_row['datetime']; 
            $_lowestLOW->value        = '$'.number_format($_row['low']);

            return $_lowestLOW;
        } 
        catch(PDOException $e)
        {
            die('PDO ERROR: ' . $e->getMessage());
        }
    }

    public function getHighestHigh()
    {
        try
        {
            $_conn = $this->getConnection();

            $_statement = $_conn->prepare(
                "SELECT high, DATE_FORMAT(updated_datetime, '%b %d, %Y %H:%i') AS datetime"
                . ' FROM `bitcoin_ticker`'
                . ' WHERE high = (SELECT MIN(high) AS highest_high'
                    . ' FROM `bitcoin_ticker` WHERE updated_datetime > :from'
                    . ' AND updated_datetime < :to)'
                .' ORDER BY updated DESC LIMIT 1');

            $_values = array(
                ':from' => $this->date_from,
                ':to'   => $this->date_to);

            $_statement->execute($_values);
            $_row = $_statement->fetch();

            $_conn = null;

            $_hightestHIGH = new stdClass();
            $_hightestHIGH->date_format  = $_row['datetime']; 
            $_hightestHIGH->value        = '$'.number_format($_row['high']);

            return $_hightestHIGH;
        } 
        catch(PDOException $e)
        {
            die('PDO ERROR: ' . $e->getMessage());
        }
    }

    public function getLowestMP()
    {
        try
        {
            $_conn = $this->getConnection();

            $_statement = $_conn->prepare(
                "SELECT buy, DATE_FORMAT(updated_datetime, '%b %d, %Y %H:%i') AS datetime"
                . ' FROM `bitcoin_ticker`'
                . ' WHERE buy = (SELECT MIN(buy) AS lowest_buy'
                    . ' FROM `bitcoin_ticker` WHERE updated_datetime > :from'
                    . ' AND updated_datetime < :to)'
                .' ORDER BY updated DESC LIMIT 1');

            $_values = array(
                ':from' => $this->date_from,
                ':to'   => $this->date_to);

            $_statement->execute($_values);
            $_row = $_statement->fetch();

            $_conn = null;

            $_lowestMarketPrice = new stdClass();
            $_lowestMarketPrice->date_format = $_row['datetime']; 
            $_lowestMarketPrice->value       = '$'.number_format($_row['buy']);

            return $_lowestMarketPrice;
        } 
        catch(PDOException $e)
        {
            die('PDO ERROR: ' . $e->getMessage());
        }
    }

    public function getHighestMP()
    {
        try
        {
            $_conn = $this->getConnection();

            $_statement = $_conn->prepare(
                "SELECT buy, DATE_FORMAT(updated_datetime, '%b %d, %Y %H:%i') AS datetime"
                . ' FROM `bitcoin_ticker`'
                . ' WHERE buy = (SELECT MAX(buy) AS highest_buy'
                    . ' FROM `bitcoin_ticker` WHERE updated_datetime > :from'
                    . ' AND updated_datetime < :to)'
                .' ORDER BY updated DESC LIMIT 1');

            $_values = array(
                ':from' => $this->date_from,
                ':to'   => $this->date_to);

            $_statement->execute($_values);
            $_row = $_statement->fetch();

            $_conn = null;

            $_highestMarketPrice = new stdClass();
            $_highestMarketPrice->date_format = $_row['datetime']; 
            $_highestMarketPrice->value       = '$'.number_format($_row['buy']);

            return $_highestMarketPrice;
        }
        catch(PDOException $e)
        {
            die('PDO ERROR: ' . $e->getMessage());
        }
    }

    public function getCurrentMPValues()
    {
        try
        {
            $_conn = $this->getConnection();

            $_statement = $_conn->prepare(
                "SELECT high, low, buy, high_last_market_diff, ABS(low_last_market_diff) AS low_last_market_diff"
                . ' FROM `bitcoin_ticker`'
                .' ORDER BY updated DESC LIMIT 1');

            $_statement->execute();
            $_row = $_statement->fetch();

            $_conn = null;

            $_currentMPValues = new stdClass();
            $_currentMPValues->high = '$'.number_format($_row['high']);
            $_currentMPValues->low = '$'.number_format($_row['low']);
            $_currentMPValues->buy = '$'.number_format($_row['buy']);
            $_currentMPValues->high_last_market_diff = $_row['high_last_market_diff']; 
            $_currentMPValues->low_last_market_diff = $_row['low_last_market_diff'];

            return $_currentMPValues;

        }
        catch(PDOException $e)
        {
            die('PDO ERROR: ' . $e->getMessage());
        }
    }

    public function getMPAvg()
    {
        try
        {
            $_conn = $this->getConnection();

            $_statement = $_conn->prepare(
                "SELECT AVG(buy) AS avg_buy"
                . ' FROM `bitcoin_ticker`'
                . ' WHERE updated_datetime > :from'
                . ' AND updated_datetime < :to LIMIT 1');

            $_values = array(
                ':from' => $this->date_from,
                ':to'   => $this->date_to);

            $_statement->execute($_values);
            $_row = $_statement->fetch();

            $_conn = null;

            return '$'.number_format($_row['avg_buy']);
        }
        catch(PDOException $e)
        {
            die('PDO ERROR: ' . $e->getMessage());
        }
    }
}